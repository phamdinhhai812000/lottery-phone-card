# Lottery-phone-card


Cách chạy dự án

1. Vào file application.properties
- Sửa địa chỉ ip tương ứng với ip máy chủ chạy container

2. Build file .jar
- mvn clean package

3. Chạy tạo image
- docker build -t app .

4. Chạy docker compose
- tải file sql "https://gitlab.com/phamdinhhai812000/data/-/blob/main/test.sql?ref_type=heads" , tạo một thư mục có chưa file mysql và sửa đường dẫn của thư mục đó vào volumes
- docker-compose up