package com.example.test.routing;

import com.example.test.data.dto.UserDTO;
import com.example.test.repository.card.ICardRepository;
import com.example.test.repository.prize.IPrizeRepository;
import com.example.test.tables.pojos.Cards;
import com.example.test.tables.pojos.Prize;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.kafka.annotation.KafkaListener;
import org.springframework.stereotype.Component;

import java.util.ArrayList;
import java.util.Comparator;
import java.util.List;
import java.util.stream.Collectors;

@Component
public class PrizeConsumerHandle {

    @Value("${spring.kafka.bootstrap-servers}")
    private String bootstrapServers;

    private final IPrizeRepository prizeRepository;
    private final ICardRepository cardRepository;

    public PrizeConsumerHandle(IPrizeRepository prizeRepository, ICardRepository cardRepository) {
        this.prizeRepository = prizeRepository;
        this.cardRepository = cardRepository;
    }

    @KafkaListener(
            topics = "${spring.kafka.topic.name}",
            groupId = "${spring.kafka.consumer.group-id}",
            containerFactory = "kafkaListenerContainerFactory")
    public void consume(List<String> messages){
        List<Cards> cards = cardRepository.getCardsValid();
        List<UserDTO> userDTOS = messages.stream().map(mess -> {
            Long userId = Long.parseLong(mess.split(":")[0]);
            Long time = Long.parseLong(mess.split(":")[1]);
            UserDTO userDTO = new UserDTO();
            userDTO.setUserId(userId);
            userDTO.setTime(time);
            return userDTO;
        }).sorted(Comparator.comparing(UserDTO::getTime))
        .collect(Collectors.toList());
        List<Prize> prizes = new ArrayList<>();
        if(cards.size() < userDTOS.size()){
            for(int i =0 ;i<cards.size();i++){
                Prize prize = new Prize();
                prize.setUserId(userDTOS.get(i).getUserId());
                prize.setCardsId(cards.get(i).getId());
                prizes.add(prize);
                cards.get(i).setStatus("invalid");
            }

            for (int i = cards.size();i<userDTOS.size();i++){
                Prize prize = new Prize();
                prize.setUserId(userDTOS.get(i).getUserId());
                prize.setCardsId(null);
                prizes.add(prize);
            }
        }else {
            for(int i =0 ;i<userDTOS.size();i++){
                Prize prize = new Prize();
                prize.setUserId(userDTOS.get(i).getUserId());
                prize.setCardsId(cards.get(i).getId());
                prizes.add(prize);
                cards.get(i).setStatus("invalid");
            }
        }
        cardRepository.insertOnDuplicateKeyUpdate(cards);
        prizeRepository.insert(prizes);
    }
}
