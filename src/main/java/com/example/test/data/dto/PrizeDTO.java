package com.example.test.data.dto;

import lombok.Data;

@Data
public class PrizeDTO {

    private Long userId;
    private String cardCode;
    private String type;
}
