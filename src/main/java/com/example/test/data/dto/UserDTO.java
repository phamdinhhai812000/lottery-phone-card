package com.example.test.data.dto;

import lombok.Data;

@Data
public class UserDTO {
    private Long userId;
    private Long time;
}
