package com.example.test.data.reponse;

import lombok.Data;

import org.springframework.http.ResponseEntity;
@Data
public class DfResponse<T> {
    private Integer code;
    private String message;
    private T result;

    public DfResponse() {
        code = 0;
        message = "OK";
    }
    public DfResponse(String message, T result) {
        this.code = 0;
        this.message = message;
        this.result = result;
    }

    public DfResponse(Integer code, String message, T result) {
        this.code = code;
        this.message = message;
        this.result = result;
    }

    public static <T> ResponseEntity<DfResponse<T>> okEntity(T body) {
        return ResponseEntity.ok(ok(body));
    }

    public static <T> DfResponse<T> ok(T body) {
        DfResponse<T> response = new DfResponse<>();
        response.setResult(body);
        return response;
    }
}
