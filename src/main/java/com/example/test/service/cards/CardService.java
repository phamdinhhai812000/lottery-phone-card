package com.example.test.service.cards;

import com.example.test.exception.ValidationException;
import com.example.test.repository.card.ICardRepository;
import com.example.test.repository.user.IUserRepository;
import com.example.test.tables.pojos.Cards;
import com.example.test.tables.pojos.User;
import com.opencsv.CSVReader;
import com.opencsv.exceptions.CsvValidationException;
import org.springframework.stereotype.Service;
import org.springframework.web.multipart.MultipartFile;

import java.io.IOException;
import java.io.InputStreamReader;
import java.util.ArrayList;
import java.util.List;

@Service
public class CardService {
    private final ICardRepository cardRepository;
    private final IUserRepository userRepository;

    public CardService(ICardRepository cardRepository, IUserRepository userRepository) {
        this.cardRepository = cardRepository;
        this.userRepository = userRepository;
    }

    public String addCards(MultipartFile file,String code) throws Exception {
        User user = userRepository.getByCode(code);
        if(!user.getRole().equals("admin")) throw new ValidationException("code","error");
        if(file.isEmpty()) return "fail";
        CSVReader reader = new CSVReader(new InputStreamReader(file.getInputStream()));
        List<Cards> cards = new ArrayList<>();
        String[] nextLine;
        boolean isFirstLine = true;
        while ((nextLine = reader.readNext())!=null){
            if(isFirstLine){
                isFirstLine = false;
                continue;
            }
            Cards card = new Cards();
            card.setCode(nextLine[0].split(";")[0]);
            card.setTelecomNetwork(nextLine[0].split(";")[1]);
            card.setType(nextLine[0].split(";")[2]);
            card.setStatus("valid");
            cards.add(card);
        }
        reader.close();
        cardRepository.insert(cards);
        return "success";
    }
}
