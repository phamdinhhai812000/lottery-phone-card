package com.example.test.service.user;

import com.example.test.data.dto.PrizeDTO;
import com.example.test.data.dto.UserDTO;
import com.example.test.exception.ValidationException;
import com.example.test.repository.card.ICardRepository;
import com.example.test.repository.prize.IPrizeRepository;
import com.example.test.repository.user.IUserRepository;
import com.example.test.tables.pojos.Cards;
import com.example.test.tables.pojos.Prize;
import com.example.test.tables.pojos.User;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.kafka.core.KafkaTemplate;
import org.springframework.stereotype.Service;

import java.time.LocalDateTime;
import java.time.ZoneId;
import java.time.ZonedDateTime;
import java.util.List;
import java.util.Random;

@Service
public class UserService {
    private final ICardRepository cardRepository;
    @Autowired
    private KafkaTemplate<String, String> kafkaTemplate;
    @Value(value = "${spring.kafka.topic.name}")
    private String topicName;

    private final IUserRepository userRepository;
    private final IPrizeRepository prizeRepository;

    public UserService(ICardRepository cardRepository, IUserRepository userRepository, IPrizeRepository prizeRepository) {
        this.cardRepository = cardRepository;
        this.userRepository = userRepository;
        this.prizeRepository = prizeRepository;
    }

    public String spin(String code) throws Exception {
        User user = userRepository.getByCode(code);
        if(user == null || !user.getRole().equals("enduser")) throw new ValidationException("code","error");
        List<String> prizes = List.of("Khong trung","Trung thuong");
        Random random = new Random();
        int index = random.nextInt(2);
        if (index == 0) return "Khong trung";
        LocalDateTime localDateTime = LocalDateTime.now();
        ZonedDateTime zdt = ZonedDateTime.of(localDateTime, ZoneId.systemDefault());
        long time = zdt.toInstant().toEpochMilli();
        kafkaTemplate.send(topicName,user.getId()+":"+time);
        return prizes.get(index);
    }

    public PrizeDTO getPrize(String code) throws Exception {
        User user = userRepository.getByCode(code);
        if(user == null || !user.getRole().equals("enduser")) throw new ValidationException("code","error");
        PrizeDTO prizeDTO = new PrizeDTO();
        Prize prize = prizeRepository.getByUserId(user.getId());
        prizeDTO.setUserId(user.getId());
        if(prize.getCardsId()==null){
            prizeDTO.setType("Het qua");
            return prizeDTO;
        }

        Cards cards = cardRepository.getByID(prize.getCardsId());
        prizeDTO.setCardCode(cards.getCode());
        prizeDTO.setType(cards.getType());
        return prizeDTO;
    }
}
