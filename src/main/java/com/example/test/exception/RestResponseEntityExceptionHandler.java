package com.example.test.exception;


import com.example.test.data.reponse.DfResponse;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.bind.annotation.RestControllerAdvice;
import org.springframework.web.context.request.WebRequest;

import java.util.HashMap;
import java.util.Map;

import static org.springframework.http.HttpStatus.*;

@RestControllerAdvice
public class RestResponseEntityExceptionHandler {

    @ExceptionHandler(ValidationException.class)
    public ResponseEntity<Object> handleValidationException(ValidationException ex) {
        Map<String, String> errors = ex.getErrors();
        // Convert keys to snake case
        Map<String, String> snakeCaseErrors = new HashMap<>();
        for (Map.Entry<String, String> entry : errors.entrySet()) {
            String snakeCaseKey = toSnakeCase(entry.getKey());
            snakeCaseErrors.put(snakeCaseKey, entry.getValue());
        }
        DfResponse response = new DfResponse(UNPROCESSABLE_ENTITY.value(), "Dữ liệu đầu vào không chính xác", snakeCaseErrors);
        return ResponseEntity.ok(response);
    }

    private String toSnakeCase(String text) {
        return text.replaceAll("([a-z])([A-Z]+)", "$1_$2").toLowerCase();
    }
}