package com.example.test.repository.prize;

import com.example.test.repository.BaseCurdRepository;
import com.example.test.tables.pojos.Prize;
import com.example.test.tables.records.PrizeRecord;
import org.jooq.impl.TableImpl;
import org.springframework.stereotype.Repository;

import java.util.List;

import static com.example.test.Tables.PRIZE;

@Repository
public class PrizeRepositoryImpl extends BaseCurdRepository<PrizeRecord, Prize,Long> implements IPrizeRepository {
    @Override
    protected TableImpl<PrizeRecord> getTable() {
        return PRIZE;
    }

    @Override
    public Prize getByUserId(Long userId) {
        return dslContext.select()
                .from(PRIZE)
                .where(PRIZE.USER_ID.eq(userId))
                .fetchOneInto(Prize.class);
    }
}
