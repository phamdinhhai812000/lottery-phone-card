package com.example.test.repository.prize;

import com.example.test.repository.ICurdRepository;
import com.example.test.tables.pojos.Prize;
import com.example.test.tables.records.PrizeRecord;

import java.util.List;

public interface IPrizeRepository extends ICurdRepository<PrizeRecord, Prize,Long> {
    Prize getByUserId(Long userId);
}
