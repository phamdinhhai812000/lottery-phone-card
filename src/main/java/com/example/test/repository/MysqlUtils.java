package com.example.test.repository;

import org.jooq.Field;
import org.jooq.impl.TableRecordImpl;

import java.util.HashMap;
import java.util.Map;

public class MysqlUtils {
    public static <T extends TableRecordImpl<T>> Map<Field<?>, Object>
    toInsertQueries(T record, Object o) {
        record.from(o);
        Map<Field<?>, Object> values = new HashMap<>();
        for (Field<?> f : record.fields()) {
            if (record.getValue(f) != null) {
                values.put(f, record.getValue(f));
            }
        }
        return values;
    }
}
