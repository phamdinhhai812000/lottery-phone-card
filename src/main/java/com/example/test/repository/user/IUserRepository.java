package com.example.test.repository.user;

import com.example.test.tables.pojos.User;

public interface IUserRepository {

    User getByCode(String code);
}
