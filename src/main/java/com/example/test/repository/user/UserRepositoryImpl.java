package com.example.test.repository.user;

import com.example.test.repository.BaseCurdRepository;
import com.example.test.tables.pojos.User;
import com.example.test.tables.records.UserRecord;
import org.jooq.impl.TableImpl;
import org.springframework.stereotype.Repository;

import static com.example.test.Tables.USER;

@Repository
public class UserRepositoryImpl extends BaseCurdRepository<UserRecord, User,Long> implements IUserRepository{
    @Override
    protected TableImpl<UserRecord> getTable() {
        return USER;
    }

    @Override
    public User getByCode(String code) {
        return dslContext.select()
                .from(USER)
                .where(USER.CODE.eq(code))
                .fetchOneInto(User.class);
    }
}
