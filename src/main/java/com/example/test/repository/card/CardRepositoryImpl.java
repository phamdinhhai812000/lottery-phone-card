package com.example.test.repository.card;

import com.example.test.data.dto.UserDTO;
import com.example.test.repository.BaseCurdRepository;
import com.example.test.tables.pojos.Cards;
import com.example.test.tables.records.CardsRecord;
import org.jooq.DSLContext;
import org.jooq.Field;
import org.jooq.InsertSetMoreStep;
import org.jooq.Record;
import org.jooq.impl.TableImpl;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

import java.lang.reflect.InvocationTargetException;
import java.lang.reflect.ParameterizedType;
import java.util.Arrays;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;

import static com.example.test.Tables.CARDS;
import static com.example.test.Tables.PRIZE;
import static com.example.test.repository.MysqlUtils.toInsertQueries;
import static com.google.common.collect.Lists.partition;

@Repository
public class CardRepositoryImpl extends BaseCurdRepository<CardsRecord, Cards, Long> implements ICardRepository {

    @Override
    protected TableImpl<CardsRecord> getTable() {
        return CARDS;
    }

    @Override
    public List<Cards> getCardsValid() {
        return dslContext.select()
                .from(CARDS)
                .where(CARDS.STATUS.eq("valid"))
                .fetchInto(Cards.class);
    }

    @Override
    public Cards getByID(Long cardId) {
        return dslContext.select()
                .from(CARDS)
                .where(CARDS.ID.eq(cardId))
                .fetchOneInto(Cards.class);
    }

}
