package com.example.test.repository.card;

import com.example.test.data.dto.UserDTO;
import com.example.test.repository.ICurdRepository;
import com.example.test.tables.pojos.Cards;
import com.example.test.tables.records.CardsRecord;

import java.util.List;

public interface ICardRepository extends ICurdRepository<CardsRecord, Cards, Long> {

    List<Cards> getCardsValid();
    Cards getByID(Long cardId);
}
