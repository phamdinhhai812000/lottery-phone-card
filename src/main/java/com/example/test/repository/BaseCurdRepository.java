package com.example.test.repository;

import com.example.test.tables.pojos.Cards;
import com.example.test.tables.records.CardsRecord;
import com.google.common.collect.Lists;
import lombok.SneakyThrows;
import org.jooq.DSLContext;
import org.jooq.Field;
import org.jooq.InsertOnDuplicateSetMoreStep;
import org.jooq.InsertSetMoreStep;
import org.jooq.impl.TableImpl;
import org.jooq.impl.TableRecordImpl;
import org.springframework.beans.factory.annotation.Autowired;

import javax.annotation.PostConstruct;
import java.lang.reflect.ParameterizedType;
import java.util.Arrays;
import java.util.Collection;
import java.util.List;
import java.util.Map;
import java.util.concurrent.atomic.AtomicReference;
import java.util.stream.Collectors;

import static com.example.test.Tables.CARDS;
import static com.example.test.repository.MysqlUtils.toInsertQueries;
import static com.google.common.collect.Lists.partition;

public abstract class BaseCurdRepository<R extends TableRecordImpl<R>, P, I> implements ICurdRepository<R, P, I> {

    @Autowired
    protected DSLContext dslContext;

    private R record;
    private Class<P> pojoClass;

    protected abstract TableImpl<R> getTable();

    @SneakyThrows
    @PostConstruct
    public void init() {
        this.record = ((Class<R>) ((ParameterizedType) getClass()
                .getGenericSuperclass()).getActualTypeArguments()[0])
                .getDeclaredConstructor()
                .newInstance();
        this.pojoClass = ((Class<P>) ((ParameterizedType) getClass()
                .getGenericSuperclass()).getActualTypeArguments()[1]);
    }

    @Override
    public List<Integer> insert(Collection<P> pojos) {
        final List<InsertSetMoreStep<R>> insertSetMoreSteps = pojos.stream()
                .map(p -> toInsertQueries(record, p))
                .map(fieldObjectMap -> dslContext
                        .insertInto(getTable())
                        .set(fieldObjectMap))
                .collect(Collectors.toList());

        return partition(insertSetMoreSteps, 100)
                .stream()
                .flatMap(lists -> Arrays.stream(dslContext.batch(lists).execute()).boxed())
                .collect(Collectors.toList());
    }

    public int insertOnDuplicateKeyUpdate(Collection<P> pojos) {
        AtomicReference<Integer> result = new AtomicReference<>(0);
        final List<InsertOnDuplicateSetMoreStep<R>> moreStepList = pojos.stream()
                .map(p -> dslContext
                        .insertInto(getTable())
                        .set(toInsertQueries(record, p))
                        .onDuplicateKeyUpdate()
                        .set(onDuplicateKeyUpdate(p)))
                .collect(Collectors.toList());

        result.set(Lists.partition(moreStepList, 5000)
                .stream()
                .mapToInt(lists -> dslContext.batch(lists).execute().length)
                .sum());
        return result.get();
    }

    protected Map<Field<?>, Object> onDuplicateKeyUpdate(P p) {
        return toInsertQueries(record, p);
    }
}
