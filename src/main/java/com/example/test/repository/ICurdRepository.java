package com.example.test.repository;

import com.example.test.tables.pojos.Cards;
import org.jooq.impl.TableRecordImpl;

import java.util.Collection;
import java.util.List;

public interface ICurdRepository<R extends TableRecordImpl<R>, P, I> {

    List<Integer> insert(Collection<P> pojos);
    int insertOnDuplicateKeyUpdate(Collection<P> pojos);
}
