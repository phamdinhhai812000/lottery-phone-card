package com.example.test.config;

import com.fasterxml.jackson.annotation.JsonProperty;
import com.zaxxer.hikari.HikariConfig;
import com.zaxxer.hikari.HikariDataSource;
import org.springframework.boot.context.properties.ConfigurationProperties;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.stereotype.Component;

@Component
@Configuration
@ConfigurationProperties("spring.datasource")
public class DataSourceApiConfig extends HikariConfig {

    @Bean
    public HikariDataSource dataSource() {
        return new HikariDataSource(this);
    }
}
