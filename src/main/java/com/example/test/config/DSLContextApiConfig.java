package com.example.test.config;

import org.jooq.DSLContext;
import org.jooq.SQLDialect;
import org.jooq.conf.Settings;
import org.jooq.impl.DSL;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.Primary;
import com.zaxxer.hikari.HikariDataSource;

@Configuration
public class DSLContextApiConfig {
    @Autowired
    private HikariDataSource dataSource;

    @Primary
    @Bean
    public DSLContext dslContext() {
        Settings settings = new Settings().withRenderSchema(true);
        return DSL.using(dataSource, SQLDialect.MYSQL, settings);
    }
}
