package com.example.test.controller.card;

import com.example.test.data.reponse.DfResponse;
import com.example.test.service.cards.CardService;
import com.opencsv.exceptions.CsvValidationException;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.multipart.MultipartFile;

import java.io.IOException;

@RestController
@RequestMapping("api/v1/admin/cards")
public class CardController {

    private final CardService cardService;

    public CardController(CardService cardService) {
        this.cardService = cardService;
    }

    @PostMapping("")
    public ResponseEntity<DfResponse<String>> addCards(@RequestParam("file") MultipartFile file, @RequestHeader("code") String code) throws Exception {
        return DfResponse.okEntity(cardService.addCards(file,code));
    }
}
