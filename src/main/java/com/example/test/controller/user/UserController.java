package com.example.test.controller.user;

import com.example.test.data.dto.PrizeDTO;
import com.example.test.data.reponse.DfResponse;
import com.example.test.service.user.UserService;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

@RestController
@RequestMapping("api/v1/user")
public class UserController {
    private final UserService userService;

    public UserController(UserService userService) {
        this.userService = userService;
    }

    @PostMapping("/spin")
    public ResponseEntity<DfResponse<String>> spin(@RequestHeader("code") String code) throws Exception {
        return DfResponse.okEntity(userService.spin(code));
    }

    @GetMapping("/get-prize")
    public ResponseEntity<DfResponse<PrizeDTO>> getPrize(@RequestHeader("code") String code) throws Exception {
        return DfResponse.okEntity(userService.getPrize(code));
    }
}
