FROM eclipse-temurin:11

VOLUME /tmp

#RUN mvn package

COPY target/*.jar app.jar


EXPOSE 8080


ENTRYPOINT ["java","-jar","/app.jar"]